from django.urls import path
from meal_plans.views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanDetailView,
    MealPlanEditView,
    MealPlanListView,
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),
    path(
        "<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_plans_detail",
    ),
    path("create/", MealPlanCreateView.as_view(), name="meal_plans_create"),
    path("<int:pk>/edit/", MealPlanEditView.as_view(), name="meal_plans_edit"),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plans_delete",
    ),
]
