from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from meal_plans.models import MealPlan
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.


class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    fields = ["name", "date", "owner", "recipes"]

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/create.html"
    fields = ["name", "date", "recipes"]
    # success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
    # Get the normal queryset
        queryset = super().get_queryset()

    # Print it to see what's in it
        print(queryset)

    # Return it like nothing ever happened
        return queryset

    # Custom handling of saving the form
    def form_valid(self, form):
        # Save the meal plan, but don't put it in the database
        plan = form.save(commit=False)
        # Assign the owner to the meal plan
        plan.owner = self.request.user
        # Now, save it to the database
        plan.save()
        # Save all of the many-to-many relationships
        form.save_m2m()
        # Redirect to the detail page for the meal plan
        return redirect("meal_plans_detail", pk=plan.id)


class MealPlanEditView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plan_detail", args=[self.object.id])


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plan_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
