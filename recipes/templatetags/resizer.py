from tkinter import N
from types import NoneType
from django import template

# from recipes.models import Ingredient

register = template.Library()

def resize_to(ingredient, target):
    # print(type(target))
    # print("FROM RESIZE: ", ingredient, "type: ", type(ingredient))

    servings = ingredient.recipe.servings
    # print("INGRED/SERV: ", indgredients_per_serving, type(indgredients_per_serving))
    try:
        if (servings is not None) and (target is not None):
            ratio = int(target) / servings
            # print(type(int(target)), type(indgredients_per_serving))
            return ratio * ingredient.amount
    except ValueError:
        return None
    return servings

    # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.serving
    # properties

    # If the servings from the recipe is not None
    #   and the value of target is not None
    # try
    # calculate the ratio of target over
    #   servings
    # return the ratio multiplied by the
    #   ingredient's amount
    # catch a possible error
    # pass
    # return the original ingredient's amount since
    #   nothing else worked


register.filter(resize_to)
