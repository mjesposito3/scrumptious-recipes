from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from recipes.forms import RatingForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.http import require_http_methods

# try:
# from recipes.forms import RecipeForm
from recipes.models import Recipe, Ingredient, FoodItem, ShoppingItem

# except Exception:
#     RecipeForm = None
#     Recipe = None


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            try:
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 4


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        
        shoplist = []
        # Create a new empty list and assign it to a variable
        for item in self.request.user.shopping_item.all():
        # For each item in the user's shopping items, which we
        # can access through the code
        # self.request.user.shopping_items.all()    
            shoplist.append(item.food_item)
            # Add the shopping item's food to the list
        context["food_in_shopping_list"] = shoplist
        context["servings"] = self.request.GET.get("servings")
         # Put that list into the context
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")
    
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


@require_http_methods(["POST"])
def create_shopping_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    # Get the value for the "ingredient_id" from the
    # request.POST dictionary using the "get" method

    # Get the specific ingredient from the Ingredient model
    # using the code
    # Ingredient.objects.get(id=the value from the dictionary)
    ingredient = Ingredient.objects.get(id=ingredient_id)

    user = request.user
    # Get the current user which is stored in request.user

    try:
        shopping_list = ShoppingItem.objects.create(
            food_item=ingredient.food, user=user
        )
        # Create the new shopping item in the database
        # using ShoppingItem.objects.create(
        #   food_item= the food item on the ingredient,
        #   user= the current user
        # )
    except IntegrityError:
        pass
    return redirect("recipe_detail", pk=ingredient.recipe.id)

    # Go back to the recipe page with a redirect
    # to the name of the registered recipe detail
    # path with code like this
    # return redirect(
    #     name of the registered recipe detail path,
    #     pk=id of the ingredient's recipe
    # )


class ShoppingItemList(ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"


def delete_all_shopping_items(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    # Delete all of the shopping items for the user
    # using code like
    # ShoppingItem.objects.filter(user=the current user).delete()
    
    return redirect("shopping_items_list")
    # Go back to the shopping item list with a redirect
    # to the name of the registered shopping item list
    # path with code like this
    # return redirect(
    #     name of the registered shopping item list path
    # )